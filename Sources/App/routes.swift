import Fluent
import Vapor

func routes(_ app: Application) throws {
    
    //MARK: Registrazione EventiController
    let eventiController = EventiController()
    try app.register(collection: eventiController)
    
    //MARK: Registrazione UtentiController
    let utentiController = UtentiController()
    try app.register(collection: utentiController)
    
    //MARK: Registrazione AuleStudioController
    let auleStudioController = AuleStudioController()
    try app.register(collection: auleStudioController)
    
    //MARK: Registrazione NewsController
    let newsController = NewsController()
    try app.register(collection: newsController)
    
    //MARK: Registrazione OrganizzatoriController
    let organizzatoriController = OrganizzatoriController()
    try app.register(collection: organizzatoriController)
}
