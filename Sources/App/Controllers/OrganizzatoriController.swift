//
//  File.swift
//  
//
//  Created by Marco Longobardi on 23/07/21.
//

import Vapor
import Fluent

struct OrganizzatoriController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        
        //Route group
        let organizzatoriRoutes = routes.grouped("api", "organizzatori")
        let tokenProtected = organizzatoriRoutes.grouped(Token.authenticator())

        
        //MARK: Routes GET
        tokenProtected.get(use: getAllHandler)
        
        //MARK: Routes POST
        tokenProtected.post(use: createHandler)
        
        //MARK: Routes UPDATE
        
        
        //MARK: Routes DELETE
        
        
    }
    
    //MARK: Funzioni per gestire le GET
    
    ///Ritorna tutti gli eventi presenti nella tabella
    func getAllHandler (_ req: Request) throws -> EventLoopFuture<[Organizzatori]> {
        try req.auth.require(Utenti.self)

        return Organizzatori.query(on: req.db).all()
    }
    
    
    //MARK: Funzioni per gestire le POST
    
    func createHandler(_ req: Request) throws -> EventLoopFuture<Organizzatori> {
        try req.auth.require(Utenti.self)

        let data = try req.content.decode(CreateOrganizzatoriData.self)
        let organizzatore = Organizzatori(mail: data.mail)
        return organizzatore.save(on: req.db).map {organizzatore}
    }

}


//MARK: DTO

///Struttura necessaria per la decodifica corretta del JSON
struct CreateOrganizzatoriData : Content {
    let mail : String
}
