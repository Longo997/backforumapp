//
//  File.swift
//  
//
//  Created by Marco Longobardi on 13/07/21.
//

import Vapor
import Fluent
import Crypto



struct PublicToken: Content {
    let token: String
}

struct NewSession: Content{
    let token: String
    let user: Utenti.IDValue
    let ruolo: String
}

extension UserSignup: Validatable {
    static func validations(_ validations: inout Validations) {
        validations.add("mail", as: String.self, is: !.empty)
        validations.add("password", as: String.self, is: .count(6...))
    }
}

struct UtentiController: RouteCollection {
    
    func boot(routes: RoutesBuilder) throws {
        
        //Route Groups
        let utentiRoutes = routes.grouped("api", "utenti")
        let tokenProtected = utentiRoutes.grouped(Token.authenticator())
        let passwordProtected = utentiRoutes.grouped(Utenti.authenticator())
        
        
        
        //MARK: Routes GET
        tokenProtected.get( ":utenteID", "eventi", use: getEventiPrenotati)
        tokenProtected.get("me", use: getMyOwnUser)
        
        //MARK: Routes POST
        utentiRoutes.post(use: create)
        passwordProtected.post("login", use: login)
        
        
    }
    
    //MARK: Funzioni per gestire le GET
    
    
    
    
    func getEventiPrenotati(_ req: Request) throws -> EventLoopFuture<[Eventi]> {
        try req.auth.require(Utenti.self)
        
        return Utenti.find(req.parameters.get("utenteID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { utente in
                
                utente.$eventi.get(on: req.db)
            }
    }
    
    
    func getMyOwnUser(req: Request) throws -> Utenti.Public {
        try req.auth.require(Utenti.self).asPublic()
    }
    
    //MARK: Funzioni per gestire le POST
    
    ///Riceve tramite JSON i dati di un utente e lo salva all'interno del database. Ritorna l'elemento salvato ed un token per l'accesso
    fileprivate func create(req: Request) throws -> EventLoopFuture<NewSession> {
        try UserSignup.validate(content: req)
        let userSignup = try req.content.decode(UserSignup.self)
        var tipo = "iscritto"
        var token: Token!
        return checkIfOrganizzatore(userSignup.mail, req: req).flatMap{ organizzatore in
            if organizzatore{
                tipo = "organizzatore"
            }
            
            guard let user = try? Utenti.create(from: userSignup,ruolo: tipo) else {
                return req.eventLoop.future(error: Abort(.internalServerError))
            }
            
            
            return checkIfUserExists(userSignup.mail, req: req).flatMap { exists in
                guard !exists else {
                    return req.eventLoop.future(error: UserError.mailTaken)
                }
                
                return user.save(on: req.db)
            }.flatMap {
                
                guard let newToken = try? user.createToken(source: .signup) else {
                    return req.eventLoop.future(error: Abort(.internalServerError))
                }
                
                token = newToken
                return token.save(on: req.db).flatMapThrowing {NewSession(token: token.value, user: try user.requireID(), ruolo: user.tipo)}
            }
        }
    }
    
    fileprivate func login(req: Request) throws -> EventLoopFuture<NewSession> {
        
        let user = try req.auth.require(Utenti.self)
        
        let token = try user.createToken(source: .login)
        
        return token
            .save(on: req.db)
            
            .flatMapThrowing {
                NewSession(token: token.value, user: try user.requireID(), ruolo: user.tipo)
            }
    }
    
    private func checkIfUserExists(_ mail: String, req: Request) -> EventLoopFuture<Bool> {
        Utenti.query(on: req.db)
            .filter(\.$mail == mail)
            .first()
            .map { $0 != nil }
    }
    
    private func checkIfOrganizzatore(_ mail: String, req: Request) -> EventLoopFuture<Bool> {
        Organizzatori.query(on: req.db)
            .filter(\.$mail == mail)
            .first()
            .map { $0 != nil }
    }
    
}

struct UserSignup: Content {
    let nome: String
    let cognome: String
    let mail: String
    let password: String
    
}

