//
//  File.swift
//  
//
//  Created by Marco Longobardi on 13/07/21.
//

import Vapor
import Fluent

struct EventiController : RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        
        //Route group
        let eventiRoutes = routes.grouped("api", "eventi")
        let tokenProtected = eventiRoutes.grouped(Token.authenticator())
        
        //MARK: Routes GET
        eventiRoutes.get(use: getAllHandler)
        eventiRoutes.get( ":eventoID", "prenotati", use: getPrenotati)
        
        //MARK: Routes POST
        tokenProtected.post(use: createHandler)
        tokenProtected.post(":eventoID", "prenotazione",":utenteID", use: aggiungiPrenotazione)
        
        //MARK: Routes UPDATE
        
        
        //MARK: Routes DELETE
        
        
    }
    
    //MARK: Funzioni per gestire le GET
    
    ///Ritorna tutti gli eventi presenti nella tabella
    func getAllHandler (_ req: Request) throws -> EventLoopFuture<[Eventi]> {
        Eventi.query(on: req.db).all()
    }
    
    
    func getPrenotati(_ req: Request) throws -> EventLoopFuture<[Utenti]> {
        Eventi.find(req.parameters.get("eventoID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { evento in
                evento.$prenotati.query(on: req.db).all()
            }
    }
    
    //MARK: Funzioni per gestire le POST
    
    func createHandler(_ req: Request) throws -> EventLoopFuture<Eventi> {
        try req.auth.require(Utenti.self)
        let data = try req.content.decode(CreateEventiData.self)
        let evento = Eventi(data: data.data, titolo: data.titolo, descrizione: data.descrizione ?? "", numPosti: data.numPosti, immagine: data.immagine)
        return evento.save(on: req.db).map {evento}
    }
    
    func aggiungiPrenotazione(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        try req.auth.require(Utenti.self)
        let utente = req.parameters.get("utenteID")

        
        return Eventi.find(req.parameters.get("eventoID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { evento in
                evento.$prenotati.query(on: req.db).all().flatMap{prenotati in
                    guard (prenotati.first(where: {$0.id?.uuidString == utente}) == nil) else {
                        return req.eventLoop.future(error: Abort(.notAcceptable))
                    }
                    let numPren = prenotati.count
                    guard numPren < evento.numPosti else {
                        return req.eventLoop.future(error: Abort(.notAcceptable))
                    }
                    
                    let eventoQuery =
                        Eventi.find(req.parameters.get("eventoID"), on: req.db)
                        .unwrap(or: Abort(.notFound))
                    let utenteQuery =
                        Utenti.find(req.parameters.get("utenteID"), on: req.db)
                        .unwrap(or: Abort(.notFound))
                    
                    return eventoQuery.and(utenteQuery)
                        .flatMap { evento, utente in
                            evento
                                .$prenotati
                                .attach(utente, on: req.db)
                                .transform(to: .created)
                        }
                }
            }
    }

}


//MARK: DTO

///Struttura necessaria per la decodifica corretta del JSON
struct CreateEventiData : Content {
    let data : String
    let titolo : String
    let descrizione : String?
    let numPosti : Int
    let immagine : String
}
