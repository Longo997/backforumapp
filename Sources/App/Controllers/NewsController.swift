//
//  File.swift
//  
//
//  Created by Marco Longobardi on 18/07/21.
//

import Vapor
import Fluent

struct NewsController : RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        
        //Route group
        let newsRoutes = routes.grouped("api", "news")
        let tokenProtected = newsRoutes.grouped(Token.authenticator())

        
        //MARK: Routes GET
        newsRoutes.get(use: getAllHandler)
        
        //MARK: Routes POST
        tokenProtected.post(use: createHandler)
        
        //MARK: Routes UPDATE
        
        
        //MARK: Routes DELETE
        
        
    }
    
    //MARK: Funzioni per gestire le GET
    
    ///Ritorna tutti gli eventi presenti nella tabella
    func getAllHandler (_ req: Request) throws -> EventLoopFuture<[News]> {
        News.query(on: req.db).all()
    }
    
    
    //MARK: Funzioni per gestire le POST
    
    func createHandler(_ req: Request) throws -> EventLoopFuture<News> {
        try req.auth.require(Utenti.self)

        let data = try req.content.decode(CreateNewsData.self)
        let news = News(dataPubblicazione: data.dataPubblicazione, titolo: data.titolo, testo: data.testo, immagine: data.immagine)
        return news.save(on: req.db).map {news}
    }

}


//MARK: DTO

///Struttura necessaria per la decodifica corretta del JSON
struct CreateNewsData : Content {
    let dataPubblicazione : String
    let titolo : String
    let testo : String
    let immagine : String
}
