//
//  File.swift
//  
//
//  Created by Marco Longobardi on 14/07/21.
//

import Vapor
import Fluent

struct AuleStudioController : RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        
        //Route group
        let auleStudioRoutes = routes.grouped("api", "auleStudio")
        let tokenProtected = auleStudioRoutes.grouped(Token.authenticator())
        
        
        //MARK: Routes GET
        auleStudioRoutes.get(use: getAllHandler)
        auleStudioRoutes.get( ":aulaID", "prenotati",":data", use: getPrenotati)
        auleStudioRoutes.get(":aulaID",":utenteID", "prenotazioni", use: getPrenotazioniUtente)
        
        
        //MARK: Routes POST
        tokenProtected.post(use: createHandler)
        tokenProtected.post(":aulaID", "prenotazione",":utenteID", use: aggiungiPrenotazione)
        
        
        //MARK: Routes UPDATE
        
        
        //MARK: Routes DELETE
        
        
    }
    
    //MARK: Funzioni per gestire le GET
    
    ///Ritorna tutte le aule studio presenti nella tabella
    func getAllHandler (_ req: Request) throws -> EventLoopFuture<[AuleStudio]> {
        AuleStudio.query(on: req.db).all()
    }
    
    func getPrenotati(_ req: Request) throws -> EventLoopFuture<[UtenteAulaPivot]> {
        let dataPren = req.parameters.get("data")
        return UtenteAulaPivot.query(on: req.db).all().map { prenotazioni in
            prenotazioni.filter{prenotazione in
                return prenotazione.data == dataPren
            }
        }
    }
    
    func getPrenotazioniUtente(_ req: Request) throws -> EventLoopFuture<[UtenteAulaPivot]> {
        let utente = req.parameters.get("utenteID")
        
        return UtenteAulaPivot.query(on: req.db).all().map { prenotazioni in
            prenotazioni.filter { prenotazione in
                return prenotazione.$utente.id.uuidString == utente
            }
        }
    }
    
    //MARK: Funzioni per gestire le POST
    
    ///Riceve tramite JSON i dati di un'aula studio e la salva all'interno del database. Ritorna l'elemento salvato
    func createHandler(_ req: Request) throws -> EventLoopFuture<AuleStudio> {
        try req.auth.require(Utenti.self)
        
        let data = try req.content.decode(CreateAuleStudioData.self)
        let aulaStudio = AuleStudio(id: data.idAula, numPosti: data.numPosti)
        return aulaStudio.save(on: req.db).map { aulaStudio }
    }
    
    func aggiungiPrenotazione(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        try req.auth.require(Utenti.self)
        
        let data = try req.content.decode(CreatePrenotazioneData.self)
        let utente = req.parameters.get("utenteID")
        return AuleStudio.find(req.parameters.get("aulaID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap{ aula in
                let posti = aula.numPosti
                return UtenteAulaPivot.query(on: req.db).all().flatMap { prenotazioni in
                    let prenData = prenotazioni.filter{prenotazione in
                        return prenotazione.data == data.data
                    }
                    let utentePrenData =  prenData.filter{prenotazione in
                        return String("\(prenotazione.$utente.id)") == utente
                    }
                    guard utentePrenData.count < 1 else {
                        return req.eventLoop.future(error: Abort(.notAcceptable))
                    }
                    guard prenData.count < posti else {
                        return req.eventLoop.future(error: Abort(.notAcceptable))
                    }
                    let aulaQuery = AuleStudio.find(req.parameters.get("aulaID"), on: req.db)
                        .unwrap(or: Abort(.notFound))
                    let utenteQuery = Utenti.find(req.parameters.get("utenteID"), on: req.db)
                        .unwrap(or: Abort(.notFound))
                    
                    return aulaQuery.and(utenteQuery)
                        .flatMap { aula, utente in
                            return try! UtenteAulaPivot(aula: aula, utente: utente, data: data.data).save(on: req.db)
                                .transform(to: HTTPStatus.created)
                        }
                }
            }
    }
}


//MARK: DTO

///Struttura necessaria per la decodifica corretta del JSON
struct CreateAuleStudioData : Content {
    let idAula: Int
    let numPosti: Int
}

struct CreatePrenotazioneData: Content{
    let data: String
}
