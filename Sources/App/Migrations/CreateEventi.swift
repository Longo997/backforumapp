//
//  File.swift
//  
//
//  Created by Marco Longobardi on 13/07/21.
//

import Fluent

struct CreateEventi: Migration {
    
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        
        database.schema("eventi")
            .id()
            .field("data", .string, .required)
            .field("titolo", .string, .required)
            .field("descrizione", .string)
            .field("numPosti", .int, .required)
            .field("immagine",.string,.required)
            .create()
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("eventi").delete()
    }
}
