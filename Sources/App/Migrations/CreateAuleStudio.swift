//
//  File.swift
//  
//
//  Created by Marco Longobardi on 14/07/21.
//

import Vapor
import Fluent
import SQLKit

struct CreateAuleStudio: Migration {
    
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema("aule-studio")
            .field("idAula", .int, .identifier(auto: false))
            .field("numPosti", .int, .required)
            
            .create()
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("aule-studio").delete()
    }
}

