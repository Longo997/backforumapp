//
//  File.swift
//  
//
//  Created by Marco Longobardi on 18/07/21.
//

import Foundation
import Fluent
import SQLKit

struct CreateNews: Migration {
    
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        
        database.schema("news")
            .id()
            .field("dataPubblicazione", .string, .required)
            .field("titolo", .string, .required)
            .field("testo", .custom(SQLRaw("TEXT")))
            .field("immagine",.string,.required)
            .create()
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("news").delete()
    }
}


