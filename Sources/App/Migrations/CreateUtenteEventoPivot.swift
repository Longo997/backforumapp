//
//  File.swift
//  
//
//  Created by Marco Longobardi on 13/07/21.
//

import Fluent

struct CreateUtenteEventoPivot: Migration {
    
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        
        database.schema("utente-evento-pivot")
            
            .id()
            .field("eventoID", .uuid, .required,
                   .references("eventi", "id", onDelete: .cascade))
            .field("utenteID", .uuid, .required,
                   .references("utenti", "id", onDelete: .cascade))
            .create()
        
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("utente-evento-pivot").delete()
    }
}
