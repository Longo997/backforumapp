//
//  File.swift
//  
//
//  Created by Marco Longobardi on 13/07/21.
//

import Fluent
import Vapor

struct CreateUtenti: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        return database.schema("utenti")
            .id()
            .field("nome", .string, .required)
            .field("cognome", .string, .required)
            .field("password", .string, .required)
            .field("mail",.string,.required)
            .field("tipo",.string,.required)
            .unique(on: "mail")
        
            .create()
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("utenti").delete()
    }
}
