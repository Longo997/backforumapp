//
//  File.swift
//  
//
//  Created by Marco Longobardi on 14/07/21.
//

import Fluent

struct CreateUtenteAulaPivot: Migration {
    
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        
        database.schema("utente-aula-pivot")
            
            .id()
            .field("aulaID", .int, .required,
                   .references("aule-studio", "idAula", onDelete: .cascade))
            .field("utenteID", .uuid, .required,
                   .references("utenti", "id", onDelete: .cascade))
            .field("data", .string, .required)
            .create()
        
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("utente-aula-pivot").delete()
    }
}
