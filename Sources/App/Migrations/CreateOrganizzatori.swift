//
//  File.swift
//  
//
//  Created by Marco Longobardi on 23/07/21.
//

import Foundation
import Fluent

struct CreateOrganizzatori: Migration {
    
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        
        database.schema("organizzatori")
            .id()
            .field("mail", .string, .required)
            .create()
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("organizzatori").delete()
    }
}

