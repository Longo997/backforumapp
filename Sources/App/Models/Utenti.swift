//
//  File.swift
//  
//
//  Created by Marco Longobardi on 13/07/21.
//

import Fluent
import Vapor

final class Utenti: Model, Content {
    static let schema = "utenti"
    
    @ID
    var id : UUID?
    
    @Field(key: "nome")
    var nome: String
    
    @Field(key: "cognome")
    var cognome: String
    
    @Field(key: "password")
    var password: String
    
    @Field(key: "mail")
    var mail: String
    
    @Field(key: "tipo")
    var tipo: String
    
    @Siblings(
        through: UtenteEventoPivot.self,
        from: \.$utente,
        to: \.$evento)
    var eventi: [Eventi]
    
    @Siblings(
        through: UtenteAulaPivot.self,
        from: \.$utente,
        to: \.$aula)
    var prenotazioniAula: [AuleStudio]
    
    ///Inizializzatore richiesto dal protocollo Model quando crea la tabella durante l'esecuzione delle Migration
    init(){}
    
    ///Inizializzatore classico
    init(id: UUID? = nil, nome: String, cognome: String, password: String, mail: String, tipo: String){
        self.id = id
        self.nome = nome
        self.cognome = cognome
        self.mail = mail
        self.password = password
        self.tipo = tipo
    }
    
    struct Public: Content {
        let nome: String
        let cognome: String
        let id: UUID
        let tipo: String
    }
}


extension Utenti {
    
    static func create(from userSignup: UserSignup, ruolo tipo: String) throws -> Utenti {
        
        Utenti(nome: userSignup.nome, cognome: userSignup.cognome, password: try Bcrypt.hash(userSignup.password), mail: userSignup.mail, tipo: tipo)
    }
    
    
    func createToken(source: SessionSource) throws -> Token {
        let calendar = Calendar(identifier: .gregorian)
        
        let expiryDate = calendar.date(byAdding: .month, value: 1, to: Date())
        
        return try Token(userId: requireID(), token: [UInt8].random(count: 16).base64, source: source,
                         expiresAt: expiryDate)
    }
    
    
    func asPublic() throws -> Public {
        Public(nome: nome, cognome: cognome,  id: try requireID(), tipo: tipo)
    }
}


extension Utenti: ModelAuthenticatable {
    
    static let usernameKey = \Utenti.$mail
    static let passwordHashKey = \Utenti.$password
    
    // 2
    func verify(password: String) throws -> Bool {
        try Bcrypt.verify(password, created: self.password)
    }
}



