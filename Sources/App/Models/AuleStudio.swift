//
//  File.swift
//  
//
//  Created by Marco Longobardi on 14/07/21.
//

import Fluent
import Vapor

final class AuleStudio: Model, Content {
    
    
    static let schema = "aule-studio"
    
    @ID(custom: "idAula", generatedBy: .user)
    var id: Int?
    
    @Field(key: "numPosti")
    var numPosti: Int
    
    @Siblings( //collegamento prenotazione aula molti a molti
      through: UtenteAulaPivot.self,
      from: \.$aula,
      to: \.$utente)
    var prenotati: [Utenti]
    
    @Children(for:\.$aula) var prenotatiPivot: [UtenteAulaPivot] //uno a molti
    
    init(){}
    
    init(id: Int, numPosti: Int){ //costruttore
        self.id = id
        self.numPosti = numPosti
    }
}
