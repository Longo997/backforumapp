//
//  File.swift
//  
//
//  Created by Marco Longobardi on 18/07/21.
//

import Foundation
import Vapor
import Fluent

final class News: Model, Content {
    static let schema = "news"
    
    @ID
    var id: UUID?
    
    @Field(key: "dataPubblicazione")
    var dataPubblicazione: String
    
    @Field(key: "titolo")
    var titolo: String
    
    @Field(key: "testo")
    var testo: String
    
    @Field(key: "immagine")
    var immagine: String
    
    init(){}

    init(dataPubblicazione: String, titolo: String, testo: String, immagine: String){
        self.dataPubblicazione = dataPubblicazione
        self.titolo = titolo
        self.testo = testo
        self.immagine = immagine
    }
}
