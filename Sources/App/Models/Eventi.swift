//
//  File.swift
//  
//
//  Created by Marco Longobardi on 13/07/21.
//

import Vapor
import Fluent

final class Eventi: Model, Content {
    static let schema = "eventi"
    
    @ID
    var id: UUID?
    
    @Field(key: "data")
    var data: String
    
    @Field(key: "titolo")
    var titolo: String
    
    @Field(key: "descrizione")
    var descrizione: String
    
    @Field(key: "numPosti")
    var numPosti: Int
    
    @Field(key: "immagine")
    var immagine: String
    

    @Siblings( //molti a molti con utente
      through: UtenteEventoPivot.self,
      from: \.$evento,
      to: \.$utente)
    var prenotati: [Utenti]
    
    ///Inizializzatore richiesto dal protocollo Model quando crea la tabella durante l'esecuzione delle Migration
    init() {}
    
    ///Inizializzatore classico
    init(id: UUID? = nil, data: String, titolo: String, descrizione: String, numPosti: Int, immagine: String) {
        self.id = id
        self.data = data
        self.titolo = titolo
        self.descrizione = descrizione
        self.numPosti = numPosti
        self.immagine = immagine
    }
    
}
