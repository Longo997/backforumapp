//
//  File.swift
//  
//
//  Created by Marco Longobardi on 13/07/21.
//

import Fluent
import Foundation
// 1
final class UtenteEventoPivot: Model {
    static let schema = "utente-evento-pivot"
    
    @ID
    var id: UUID?
    
    @Parent(key: "eventoID")
    var evento: Eventi
    
    @Parent(key: "utenteID")
    var utente: Utenti
    
    init() {}
    
    init(
        id: UUID? = nil,
        evento: Eventi,
        utente: Utenti
    ) throws {
        self.id = id
        self.$evento.id = try evento.requireID()
        self.$utente.id = try utente.requireID()
    }
    
}
