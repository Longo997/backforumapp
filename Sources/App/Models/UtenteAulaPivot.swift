//
//  File.swift
//  
//
//  Created by Marco Longobardi on 15/07/21.
//

import Foundation
import Vapor
import Fluent

final class UtenteAulaPivot : Model, Content {
    static let schema = "utente-aula-pivot"
    
    @ID
    var id : UUID?
    
    @Parent(key: "aulaID")
    var aula: AuleStudio
    
    @Parent(key: "utenteID")
    var utente: Utenti
    
    @Field(key: "data")
    var data: String
    
    init() {}
    
    init(id: UUID? = nil, aula: AuleStudio, utente: Utenti, data: String) throws {
        self.id = id
        self.$aula.id = try aula.requireID()
        self.$utente.id = try utente.requireID()
        self.data = data
    }
}
