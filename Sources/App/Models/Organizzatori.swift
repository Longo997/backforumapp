//
//  File.swift
//  
//
//  Created by Marco Longobardi on 23/07/21.
//

import Foundation
import Vapor
import Fluent

final class Organizzatori: Model, Content {
    static let schema = "organizzatori"
    
    @ID
    var id: UUID?
    
    @Field(key: "mail")
    var mail: String
    
    init(){}

    init(mail: String){
        self.mail = mail
    }
}
