//
//  File.swift
//  
//
//  Created by Marco Longobardi on 15/07/21.
//

import Vapor

enum UserError {
    case mailTaken
    case internalError
}

extension UserError: AbortError {
    var description: String {
        reason
    }
    
    var status: HTTPResponseStatus {
        switch self {
        case .mailTaken: return .conflict
        case .internalError: return .badRequest
        }
    }
    
    var reason: String {
        switch self {
        case .mailTaken: return "Mail già utilizzata"
        case .internalError: return "Error"
        }
    }
}
