import Fluent
import FluentPostgresDriver
import Vapor

// configures your application
public func configure(_ app: Application) throws {
    // uncomment to serve files from /Public folder
    // app.middleware.use(FileMiddleware(publicDirectory: app.directory.publicDirectory))
    if let databaseURL = Environment.get("DATABASE_URL"), var postgresConfig = PostgresConfiguration(url: databaseURL) {
        postgresConfig.tlsConfiguration = .forClient(certificateVerification: .none)
        app.databases.use(.postgres(
            configuration: postgresConfig
        ), as: .psql)
        
    }
    else {
    app.databases.use(.postgres(
        hostname: Environment.get("DATABASE_HOST") ?? "localhost",
        port: Environment.get("DATABASE_PORT").flatMap(Int.init(_:)) ?? PostgresConfiguration.ianaPortNumber,
        username: Environment.get("DATABASE_USERNAME") ?? "vapor_username",
        password: Environment.get("DATABASE_PASSWORD") ?? "vapor_password",
        database: Environment.get("DATABASE_NAME") ?? "vapor_database"
    ), as: .psql)}

    app.migrations.add(CreateEventi())
    app.migrations.add(CreateUtenti())
    app.migrations.add(CreateUtenteEventoPivot())
    app.migrations.add(CreateAuleStudio())
    app.migrations.add(CreateUtenteAulaPivot())
    app.migrations.add(CreateTokens())
    app.migrations.add(CreateNews())
    app.migrations.add(CreateOrganizzatori())
    
    app.logger.logLevel = .debug

    try app.autoMigrate().wait()
    
    // register routes
    try routes(app)
}
